#!/usr/bin/env python2
from mpi4py import MPI
import sys
import Sherpa

# Add this to the execution arguments to prevent Sherpa from starting the cross section integration
sys.argv.append('INIT_ONLY: 2')

Generator=Sherpa.Sherpa(len(sys.argv),sys.argv)
try:
    Generator.InitializeTheRun()
    Process=Sherpa.MEProcess(Generator)

    # Incoming flavors must be added first!
    Process.AddInFlav(21);
    Process.AddInFlav(21);
    Process.AddOutFlav(23);
    Process.AddOutFlav(23);
    Process.Initialize();

    # First argument corresponds to particle index:
    # index 0 correspons to particle added first, index 1 is the particle added second, and so on...
    # Process.SetMomentum(0, 6500.0,0.0,0.0,6500.0)
    # Process.SetMomentum(1, 6500.0,0.0,0.0,-6500.0)
    # Process.SetMomentum(2, 6500.0,0.0,6498.79796578,0.0)
    # Process.SetMomentum(3, 6500.0,0.0,-6498.7979657,0.0)
 
    # Random momenta
    E_cms = 6500.0
    wgt = Process.TestPoint(E_cms)
    print '\nRandom test point '
    print 'Squared ME: ', Process.CSMatrixElement(), '\n'

except Sherpa.SherpaException as exc:
    print exc
    exit(1)
