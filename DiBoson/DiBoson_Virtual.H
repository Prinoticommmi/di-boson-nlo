#include "PHASIC++/Process/Virtual_ME2_Base.H"

namespace MODEL {
  class Running_AlphaS;
}
 
namespace DiBoson {

  class DiBoson_Virtual : public PHASIC::Virtual_ME2_Base {

  public:

    DiBoson_Virtual(const PHASIC::Process_Info& pi,
                    const ATOOLS::Flavour_Vector& flavs);

    ~DiBoson_Virtual(){};
  
    bool SetColours(const ATOOLS::Vec4D_Vector& momenta) { return true; }
    void Calc(const ATOOLS::Vec4D_Vector& p);
    void Calc(const ATOOLS::Vec4D_Vector& p, const double& Born);
    double Eps_Scheme_Factor(const ATOOLS::Vec4D_Vector& p);

  private:

    ATOOLS::Flavour_Vector m_flavs;
    int process_type;
    double m_symfac;
    double m_aqed;
    MODEL::Running_AlphaS* p_as;
    
    double m_beta0;
    double m_prefactor;

  };

}

