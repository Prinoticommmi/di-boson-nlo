#include "ggvvamp.h"
#include "Precise_Amp.H"
#include "SHERPA/Main/Sherpa.H"
#include "AddOns/Python/MEProcess.H"
#include "MODEL/Main/Model_Base.H"

class DiBosonNLO {


    double M32, M42;
    int Nf;
    const double MW = 80.379;
    const double MZ = 91.1876;
    const double WZ = 0;//2.4952;
    const double WW = 0;//2.085;
    const double cos2w	= MW*MW/(MZ*MZ);
    const double sin2w	= 1 - cos2w;
    const double alphaqed = 1/128.; 
    const double alphas = 0.118;


    public:


    DiBosonNLO(double m32, double m42, int nf);

    ~DiBosonNLO(){};
    Precision::Precise_Amp F;
    double Calc(const ATOOLS::Vec4D_Vector& p);
    double S2(const ATOOLS::Vec4D_Vector& p, int nLoops);
    std::vector< std::complex<double> > decay(const int c, const double Q2);
};
