THIS REPO IS STILL A WIP
------------------------

This repository is meant to build a possible interface of ggvvamp package with Sherpa. 

The full package can be found at this link:
  https://vvamp.hepforge.org

The package require also to have installed Cln:

```
  wget https://www.ginac.de/CLN/cln-1.3.4.tar.bz2
  tar -xjf cln-1.3.4.tar.bz2
  rm cln-1.3.4.tar.bz2 
  cd cln-1.3.4
  ./configure --prefix=/path/to/ginac/and/cln
  make
  make check
  make isntall
```

and GiNaC:

```
  wget https://ginac.de/ginac-1.7.7.tar.bz2
  tar -xjf ginac-1.7.7.tar.bz2
  rm ginac-1.7.7.tar.bz2
  cd ginac-1.7.7
  ./configure --prefix=/path/to/ginac/and/cln
  make
  make install
```



NOTE: GiNaC and Cln must be installed with the same PREFIX. Read also the INSTALL file of both as long as there can be some infos about system dependencies




Authors of ggvvamp package
--------------------------

"The two-loop helicity amplitudes for gg -> V1V2 -> 4 leptons"
A. von Manteuffel, L. Tancredi


Summary:
--------

This package supplies a C++ implementation for the numerical evaluation
of the two-loop amplitude coefficients described in the paper. The
code is organised in form of a shared library:

  libggvvamp.so
  
A simple command line interface is provided by the command line program

  evalcmdlinegg
  
which employs the shared library libggvamp.so.


File content:
-------------

Please see
  evalcmdlinegg.cpp
for an easy usage example and
  ggvvamp.h
  ggvvampprec.h
for reference documentation. The files
  ggvvamp.cpp,
  ggvvampprec.cpp
provide internal information.
