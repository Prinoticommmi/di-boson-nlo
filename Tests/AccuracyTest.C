#ifdef ACC_TEST

#include "DiBoson.H"
#include <iostream>
#include <iomanip>
#include "SHERPA/Main/Sherpa.H"
#include <cmath>
#include <stdlib.h>


namespace Test{
    void AccTest(double M, int procSelect, int nFlav, double sqrts, double slices)
    {
        double M2 = M*M;
        for(int i=0; i<6; i+=1){
            double k = sqrt( sqrts*sqrts*sqrts*sqrts + 2.*M2*M2 - 2.*(2.*sqrts*sqrts*M2 + M2*M2) );
            double tM   = 0.5*(2.*M2 - sqrts*sqrts + k);
            double tm   = 0.5*(2.*M2 - sqrts*sqrts - k);
            double TM   = (tM + tm)/slices; 
            double t    = tm - (i+1)*TM;
            double u    = 2.*M2 - sqrts*sqrts - t;
            double E3   = sqrts/2.;
            double E4   = sqrts/2.;
            double p    = sqrt( (2*M2 - sqrts*sqrts)*(2*M2 - sqrts*sqrts) - 4.*M2*M2)/(sqrts*2.);
            double cos_t= (sqrts*sqrts + 2*t - 2.*M2)/(p*sqrts*2.);
            double sin_t= sqrt((t*u - M2*M2))/(p*sqrts);
            double eta = -log( (1 - cos_t) / (1 + cos_t) )*0.5;

            ATOOLS::Vec4D_Vector P;
            ATOOLS::Vec4D p1(sqrts/2., 0., 0., sqrts/2.);
            P.push_back(p1);
            ATOOLS::Vec4D p2(sqrts/2., 0., 0., -sqrts/2.);
            P.push_back(p2);
            ATOOLS::Vec4D p3(E3, p*sin_t, 0., p*cos_t);
            P.push_back(p3);
            ATOOLS::Vec4D p4(E4, -p*sin_t, 0., -p*cos_t);
            P.push_back(p4);


            DiBosonNLO amp(M2, M2, nFlav);


            double damp     = amp.Calc(P);

            std::cout << std::setprecision(15) << eta << damp << std::endl;

        }
    }
}

#endif
